import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';
import { SettingsProvider } from '../providers/settings';

@Injectable()
export class LecturesProvider {

  times: any[] = []
  lecturesVersion: string;
  lecturesData: any;
  initial: any = null;
  source: String = "";
  constructor(public http: Http, public storage: Storage, public settings: SettingsProvider) {
    console.log("Lectures Provider instatiated");
    if (!this.initial){
    storage.ready().then(() => {

        storage.get("lecturesVersion").then((version) => {
          if (version){
            console.log('lectures version: '+ version);
            this.lecturesVersion = version;
            this.storage.get("lecturesData").then((schedule) => {
              this.lecturesData = JSON.parse(schedule);
              this.source = "localstorage"
            });
         } else {
              console.log('no lectures data: new install');
              http.get('assets/data/lectures.json').map(res => res.json()).subscribe(
                result => {
                  this.lecturesData=result.schedule;
                  this.lecturesVersion = result.version;
                  this.initial = true;
                  console.log("Lectures loaded from json");
                  this.storeData();
                  this.source = "json"
                },
                err =>{
                  console.error("Error : "+err);
                } ,
                () => {
                  // this.room = tlecturesData[0].name
                  console.log('getData completed');
                }
              );

              }
        });
        });

      }
  }

  storeData(){
    this.storage.set('lecturesData',JSON.stringify(this.lecturesData));
    this.storage.set('lecturesVersion',this.lecturesVersion);
  }

  sync(){
  this.http.get(this.settings.data.lecturesURL).map(res => res.json()).subscribe(
    result => {
      this.lecturesData=result.schedule;
      this.lecturesVersion = result.version;
      console.log("Lectures loaded from url");
      this.storage.set('lecturesData',JSON.stringify(this.lecturesData));
      this.storage.set('lecturesVersion',this.lecturesVersion);
      this.source = "url"
    },
    err =>{
      console.error("Error : "+err);
    } ,
    () => {
      console.log('lectures getData completed');
    }
  );
  }

  cardClass(lecture){
     switch(lecture.fs){
        case "Psychologie":{
          return "card-red";
        }
        case "Chemie":{
          return "card-amber";
        }
        case "Biochemie":{
          return "card-lightgreen";
        }
        case "Mathematik":{
          return "card-bluegrey";
        }
        case "Geowissenschaften":{
          return "card-orange";
        }
        case "Medizin":{
          return "card-cyan";
        }
        case "Biowissenschaften":{
          return "card-green";
        }
        case "Pharmazie":{
          return "card-green";
        }
        case "Biophysik":{
          return "card-teal";
        }
        case "Physik":{
          return "card-indigo";
        }
        case "Informatik":{
          return "card-pink";
        }
        default: {
          return "card-blue";
        }
     }
  }
  fsIcon(lecture){
     switch(lecture.fs){
        case "Psychologie":{
          return "icon-psychologie";
        }
        case "Chemie":{
          return "icon-chemie";
        }
        case "Biochemie":{
          return "icon-biochemie";
        }
        case "Mathematik":{
          return "icon-mathematik";
        }
        case "Geowissenschaften":{
          return "icon-geo";
        }
        case "Medizin":{
          return "icon-medizin";
        }
        case "Biowissenschaften":{
          return "card-green";
        }
        case "Pharmazie":{
          return "icon-pharmazie";
        }
        case "Physik":{
          return "icon-physik";
        }
        case "Informatik":{
          return "icon-informatik";
        }
        default: {
          return "icon-nos";
        }
     }
  }

  toggleFavourite(lecture){
    if (lecture.favourite){
      lecture.favourite=false;
      }else{
      lecture.favourite=true;
      }
    this.storeData();
  }

}

export function LecturesProviderFactory(http: Http, storage: Storage){

}
