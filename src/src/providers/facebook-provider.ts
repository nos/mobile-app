import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import { Storage } from '@ionic/storage';
import { SettingsProvider } from '../providers/settings';

@Injectable()
export class FacebookProvider {
  fb_token: string;
  initial: any = null;
  feedData: any = null;
  feedVersion: string = ""
  source: string = ""
  constructor(public http: Http, public storage: Storage, public settings: SettingsProvider) {
    console.log('Hello FacebookProvider Provider');
    if (!this.initial){
    storage.ready().then(() => {

        storage.get("feedVersion").then((version) => {
          if (version){
            console.log('feed version: '+ version);
            this.feedVersion = version;
            this.storage.get("feedData").then((feed) => {
              this.feedData = JSON.parse(feed);
              this.source = "localstorage"
            });
         } else {
              console.log('no feed data: new install');
              http.get('assets/data/feed.json').map(res => res.json()).subscribe(
                result => {
                  this.feedData=result.feed.data;
                  console.log('blubb '+this.feedData);
                  this.feedVersion = result.version;
                  this.initial = true;
                  console.log("Lectures loaded from json");
                  this.storage.set('feedData',JSON.stringify(this.feedData));
                  this.storage.set('feedVersion',this.feedVersion);
                  this.source = "json"
                },
                err =>{
                  console.error("Error : "+err);
                } ,
                () => {
                  console.log('Facebook getData completed');
                }
              );

              }
        });
        });

      }


  }

  getToken(){
      return this.http.get('https://graph.facebook.com/oauth/access_token?client_id=383921551657881&client_secret=4343cb687dc79a236dcf19ae032e81ec&grant_type=client_credentials').map(res => res.json());
  }

  getFeed(){
  return this.http.get('https://graph.facebook.com/oauth/access_token?client_id=383921551657881&client_secret=4343cb687dc79a236dcf19ae032e81ec&grant_type=client_credentials').map(res => res.json())
  .flatMap((token) => {
      console.log('token'+token.access_token);
      return this.http.get('https://graph.facebook.com/nightofscience/feed?access_token='+token.access_token).map(res => res.json());
    })


  }

}
