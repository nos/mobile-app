import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class SettingsProvider {
  initial: any = null;
  version: number = null;
  data: {[setting: string]:string} = null;
  constructor(public storage: Storage) {
  if (!this.initial){
  storage.ready().then(() => {

      storage.get("version").then((version) => {
        if (version){
          console.log('settings version: '+ version);
          this.version = version;
          storage.get("data").then((settings) => {
            this.data = JSON.parse(settings);
          });
       } else {
            console.log('no settings data: new install');
            this.version = version;
            this.data = {};
            this.data['lecturesURL'] = "https://test.wonko.de/lectures.json";
            this.version = 1;
            storage.set('data',JSON.stringify(this.data));
            storage.set('version',this.version);
            }
      });
      });

    }
  }

}
