import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { Storage, IonicStorageModule } from '@ionic/storage';
import { Http } from '@angular/http';
import { MyApp } from './app.component';
import { Tabs } from '../pages/tabs/tabs';
import { News } from '../pages/news/news';
import { Programme } from '../pages/programme/programme';
import { Tours } from '../pages/tours/tours';
import { Campus } from '../pages/campus/campus';
import { Imprint } from '../pages/imprint/imprint';
import { Sponsors } from '../pages/sponsors/sponsors';
import { Settings } from '../pages/settings/settings';


import { LecturesProvider } from '../providers/lectures-provider';
import { FacebookProvider } from '../providers/facebook-provider';
import { SettingsProvider } from '../providers/settings';

let pages = [
MyApp,
Tabs,
News,
Programme,
Tours,
Campus,
Imprint,
Sponsors,
Settings
];


export function declarations() {
  return pages;
}

export function entryComponents() {
  return pages;
}

export function providers() {
  return [
    SplashScreen,
    StatusBar,
    FacebookProvider,
    LecturesProvider,
    SettingsProvider,
    // { provide: LecturesProvider, useFactory: LecturesProviderFactory, deps: [Http,Storage] },
    //{ provide: Settings, useFactory: provideSettings, deps: [Storage] },
    // Keep this to enable Ionic's runtime error handling during development
    //{ provide: ErrorHandler, useClass: IonicErrorHandler }
  ];
}

@NgModule({
  declarations: declarations(),
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: entryComponents(),
  providers: providers()
})
export class AppModule { }
