import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';

import { Tabs } from '../pages/tabs/tabs';

import { Imprint } from '../pages/imprint/imprint';
import { Settings } from '../pages/settings/settings';
import { Sponsors } from '../pages/sponsors/sponsors';

import { LecturesProvider } from '../providers/lectures-provider';
import { SettingsProvider } from '../providers/settings';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage = Tabs;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, private statusBar: StatusBar, private splashScreen: SplashScreen, private storage: Storage, private lecturesProvider: LecturesProvider, private settings: SettingsProvider) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      // { title: 'Einstellungen', component: Settings },
      { title: 'Impressum', component: Imprint },
      { title: 'Sponsoren', component: Sponsors }
    ];

  }
  openPage(page) {
    this.nav.push(page.component);
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.initStorage();
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  initStorage(){
    this.storage.ready().then(() => {


       });
     }
}
