import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { LecturesProvider } from '../../providers/lectures-provider';
import { ViewChild } from '@angular/core';
import { Slides } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

@Component({
  selector: 'page-programme',
  templateUrl: 'programme.html'
})
export class Programme {
  @ViewChild(Slides) slides: Slides;
  room: any = "";
  rooms: any[] = [];
  times: any[] = [];
  loading: any;
  lecturesData: any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl: AlertController, public lecturesProvider: LecturesProvider,  public loadingCtrl: LoadingController) {
  this.loading = this.loadingCtrl.create({
    content: `
      <ion-spinner ></ion-spinner>`
  });
  console.log("using lecturesprovider" + this.lecturesProvider);
  //this.lecturesData = this.lecturesProvider.lecturesData;
    this.room = this.lecturesProvider.lecturesData[0].name;
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad Programme');
  }

  cardClass(lecture){
     switch(lecture.fs){
        case "Psychologie":{
          return "card-red";
        }
        case "Chemie":{
          return "card-amber";
        }
        case "Biochemie":{
          return "card-lightgreen";
        }
        case "Mathematik":{
          return "card-bluegrey";
        }
        case "Geowissenschaften":{
          return "card-orange";
        }
        case "Medizin":{
          return "card-cyan";
        }
        case "Biowissenschaften":{
          return "card-green";
        }
        case "Pharmazie":{
          return "card-green";
        }
        case "Biophysik":{
          return "card-teal";
        }
        case "Physik":{
          return "card-indigo";
        }
        case "Informatik":{
          return "card-pink";
        }
        default: {
          return "card-blue";
        }
     }
  }
  fsIcon(lecture){
     switch(lecture.fs){
        case "Psychologie":{
          return "icon-psychologie";
        }
        case "Chemie":{
          return "icon-chemie";
        }
        case "Biochemie":{
          return "icon-biochemie";
        }
        case "Mathematik":{
          return "icon-mathematik";
        }
        case "Geowissenschaften":{
          return "icon-geo";
        }
        case "Medizin":{
          return "icon-medizin";
        }
        case "Biowissenschaften":{
          return "card-green";
        }
        case "Pharmazie":{
          return "icon-pharmazie";
        }
        case "Physik":{
          return "icon-physik";
        }
        case "Informatik":{
          return "icon-informatik";
        }
        default: {
          return "icon-nos";
        }
     }
  }

  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();
    this.room = this.lecturesProvider.lecturesData[currentIndex].name;
  }

  showAlert() {
    let alert = this.alertCtrl.create({
      title: 'Programminformationen',
      subTitle: 'Version: '+ this.lecturesProvider.lecturesVersion +'<br/>Source: '+ this.lecturesProvider.source,
      buttons: ['OK']
    });
    alert.present();
  }


}
