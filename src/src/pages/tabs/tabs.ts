import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { News } from '../news/news';
import { Programme } from '../programme/programme';
import { Tours } from '../tours/tours';
import { Campus } from '../campus/campus';


@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class Tabs {

  newsRoot: any = News;
  programmeRoot: any = Programme;
  toursRoot: any = Tours;
  campusRoot: any = Campus;



  constructor(public navCtrl: NavController, public navParams: NavParams) {}


}
