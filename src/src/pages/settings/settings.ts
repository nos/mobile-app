import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { LecturesProvider } from '../../providers/lectures-provider';
import { SettingsProvider } from '../../providers/settings';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class Settings {

  constructor(public navCtrl: NavController, public navParams: NavParams, public lecturesProvider: LecturesProvider, public settings: SettingsProvider) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Settings');
  }

}
