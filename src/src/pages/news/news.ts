import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FacebookProvider } from '../../providers/facebook-provider';


@Component({
  selector: 'page-news',
  templateUrl: 'news.html'
})
export class News {

  fb_feed: any;
  constructor(public navCtrl: NavController, private facebookProvider: FacebookProvider, public navParams: NavParams) {
    console.log("using feedprovider" + this.facebookProvider);
    console.log("data" + this.facebookProvider.feedData);
  }
  getdata(){
  this.facebookProvider.getFeed().subscribe(
    result => {
      this.fb_feed=result.data;
      console.log("Success : "+this.fb_feed);
    },
    err =>{
      console.error("Error : "+err);
    } ,
    () => {
      console.log('getData completed');
    }
  );

  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad News');
  }

}
