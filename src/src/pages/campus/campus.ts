import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-campus',
  templateUrl: 'campus.html',
})
export class Campus {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Campus');
  }

  showMap(){
  //this.photoViewer.show('http://nightofscience.de/mm/Riedberg_Lageplan_2015_HP_komplett_weiss.png', 'My image title', {share: false});
  }

}
