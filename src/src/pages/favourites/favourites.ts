import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LecturesProvider } from '../../providers/lectures-provider';

@IonicPage()
@Component({
  selector: 'page-favourites',
  templateUrl: 'favourites.html',
})
export class FavouritesPage {

  lectures = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public lecturesProvider: LecturesProvider) {
  this.getFavs();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Favourites');
  }
 getFavs(){
    console.log('getfavs');

    for (let room of this.lecturesProvider.lecturesData ){
        for (let time of room.times){
            if(time.lecture && time.lecture.favourite){
                this.lectures.push(time.lecture)
                console.log('lecture fav: ' + time.lecture.room);
            }
        }
    }
 }
}
