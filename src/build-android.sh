#!/bin/bash

export ANDROID_HOME=/home/wonko/android-sdk-linux
ionic cordova build android --aot --prod --optimizejs --minifyjs --minifycss --release 
cp platforms/android/build/outputs/apk/android-release.apk ~/git/plumbing/fdroid-repo/repo/nos.apk
