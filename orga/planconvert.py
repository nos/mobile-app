import gspread
import json
from oauth2client.service_account import ServiceAccountCredentials

scope = ['https://spreadsheets.google.com/feeds']

credentials = ServiceAccountCredentials.from_json_keyfile_name('gsheets.json', scope)


gc = gspread.authorize(credentials)

ws = gc.open("vortragsplan-beta").sheet1


print(ws.updated)

rooms = []
times = []

data = []


def cval(cell):
    cv = cell.value
    if cv:
        cv = cv.strip()
    return cv

for cell in ws.range(1,1,1,20):
    room = cval(cell)
    if room:
        room = room.strip()
        if room:
            rooms.append(cell)

for cell in ws.range(1,1,200,1):
    time = cval(cell)
    if time:
        times.append(cell)


for room in rooms:
    roomdata = {'name':room.value}
    timeslots = []
    for time in times:
        timename = time.value
        be = timename.split('-')
        timeslot = {'time': time.value, 'begin': be[0].strip(), 'end':be[1].strip()}

        lecture = {}
        col = room.col
        row = time.row
        fs = cval(ws.cell(row=row, col=col))
        if fs:
            lecture['fs'] = fs
        titel = cval(ws.cell(row=row + 1, col=col))
        if titel:
            lecture['titel'] = titel
        name = cval(ws.cell(row=row + 2, col=col))
        if name:
            lecture['name'] = name
        teaser = cval(ws.cell(row=row + 3, col=col))
        if teaser:
            lecture['teaser'] = teaser
        aufz = cval(ws.cell(row=row + 4, col=col))
        if aufz:
            if aufz.lower() == "ja":
                lecture['aufz'] = True
        
        if len(lecture) > 0:
            lecture['time'] = time.value
            lecture['room'] = room.value
            lecture['id'] = titel
            timeslot['lecture'] = lecture
        
        timeslots.append(timeslot)
    roomdata['times'] = timeslots
    data.append(roomdata)
print (data)
jsondata = {'schedule':data, 'version': ws.updated}
with open('lectures.json', 'w') as outfile:
    json.dump(jsondata, outfile, indent=1)
